<?php

    $output = [
        'code'  =>  0,
        'status'    =>  'error',
        'message'   =>  'Bad request',
    ];

    try {
        $request = '';
        
        if ( isset( $_POST[ 'video' ] ) && !empty( $_POST[ 'video' ] ) &&
            isset( $_POST[ 'audio' ] ) && !empty( $_POST[ 'audio' ] )  ) {
            $request = $_POST;
        } else {
            return $output;
        } // end if else

        foreach ( $request as $key => $value ) {
           
            if ( $key == 'video' )
                $file_name = 'video' . uniqid() . 'mpg';
                $video = str_replace( 'data:application/video;base64,', '', $value );
                \File::put( $url . '/src_video' . $file_name, base64_decode( $video ) );
            else 
                $file_name = 'audio' . uniqid() . 'aac';
        

            if ( $key =='first_video' )
                $first_video = $file_name;
            else
                $second_video = $file_name;
        } // end foreach
        unset( $value );

        $query = "ffmpeg -i src_video/Video1.mpg -i src_video/Audion.aac"
        -map 0:v:0 -map 1:a:0 results/video_audio.mpg

    } catch ( Exception $error ) {
        echo $error->getMessage();
    }