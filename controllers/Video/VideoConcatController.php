<?php

    $output = [
        'code'  =>  0,
        'status'    =>  'error',
        'message'   =>  'Bad request',
    ];

    try {
        $request = [];
        $first_video = '';
        $second_video = '';
        $url = $_SERVER['DOCUMENT_ROOT'];

        if ( isset( $_POST[ 'first_video' ] ) && !empty( $_POST[ 'first_video' ] ) &&
            isset( $_POST[ 'second_video' ] ) && !empty( $_POST[ 'second_video' ] )  ) {
            $request = $_POST;
        } else {
            return $output;
        } // end if else

        foreach ( $request as $key => $value ) {
            $file_name = 'video' . uniqid() . '.mpg';
            // $video = str_replace( 'data:application/video;base64,', '', $value );
            // \File::put( $url . '/src_video' . $file_name, base64_decode( $video ) );
            file_put_contents( $url . '/src_video/' . $file_name, $value );

            if ( $key =='first_video' )
                $first_video = $url . '/src_video/' . $file_name;
            else
                $second_video = $url . '/src_video/' . $file_name;
        } // end foreach
        unset( $value );

        $intermediate = 'video' . uniqid() . 'mpg';
        $folder = $url . '/intermediate/' . $intermediate;
        $result = $url . '/results/' . $intermediate;

        exec( "cat $first_video $second_video > $folder" );
        exec( "ffmpeg -i $folder -qscale:v 2 $result" );

        $output = [
            'code'  =>  200,
            'status'    =>  'success',
            'message'   =>  'Operation Successful',
            'data'  =>  $result,
        ];

    } catch ( Exception $error ) {
        echo $error->getMessage();
    }

    return $output;